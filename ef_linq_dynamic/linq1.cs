using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF1
{
    class Program
    {
        static IEnumerable<object> GetEmpyNameId(StudyEntities1 se)
        {
            var query1 = from _emp in se.Employees
                         select new { ID = _emp.ID, NAME = _emp.NAME };
            return query1.ToList();
            
        }

        static void Main(string[] args)
        {

            // Table:
            // Orders:
            // ID int identity
            // PRODUCT text
            // PRICE real
            // QUANTITY int
            // ADDRESS_COUNTRY text
            // --- place 5 orders
            // EF --> connect C# DB first
            // print all orders
            // create function which gets all orders PRICE, QUANTITY, TOTAL (price*quantity)
            // call this function and print values using dynamic + json serialize

            using (StudyEntities1 se = new StudyEntities1())
            {

                var enumar = GetEmpyNameId(se);
                foreach (var _empyNameId in enumar)
                {
                    Console.WriteLine(((dynamic)_empyNameId).ID);
                    Console.WriteLine(((dynamic)_empyNameId).NAME);
                }

                enumar.ToList().ForEach(_ => Console.WriteLine(JsonConvert.SerializeObject(_)));

                int[] nums = { 1, 2, 3 };
                int item = nums.ToList().OrderByDescending(_ => _).First(_ => _ > 1);
                var resul1 = (from n in nums.ToList()
                              where n > 1
                              orderby n descending
                              select (n + 1)).ToList();


                // lazy vs eager
                var empp = from _emp in se.Employees
                                      select _emp;

                var result = empp.ToList();

                var query1 = from _emp in se.Employees
                             select new { ID = _emp.ID, NAME = _emp.NAME };

                var x1 = new { Id = 1 };

                Console.WriteLine("=====================================");

                var query1_res = query1.ToList();


            Console.ReadLine();
        }
    }
}
